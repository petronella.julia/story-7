from django.test import TestCase
from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client
from .views import index

# Create your tests here.
class UnitTestLab7(TestCase):
    def test_access(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_index_in_views(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_html_template(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'about.html')

    def test_tesalonika(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Tesalonika Julia", content)

    def test_desc(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Tentang saya", content)

    def test_java_script(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<script", content)

    def test_jquery(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", content)

    def test_activity(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Aktivitas", content)

    def test_activity_content(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Saya adalah mahasiswi semester 3 program studi S1 Sistem Informasi di Fakultas Ilmu Komputer, Universitas Indonesia. Aktivitas saya saat ini selain belajar adalah mengikuti beberapa organisasi dan kepanitiaan di kampus.",content)

    def test_experience(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Organisasi dan Kepanitiaan", content)   

    def test_experience_content(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Bendahara DPM Fasilkom UI 2019", content) 
        self.assertIn("Bendahara Pemira IKM Fasilkom UI 2019", content) 
        self.assertIn("Bendahara BETIS Fasilkom UI 2020", content) 

    def test_achievement(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Prestasi", content) 

    def test_achievement_content(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Juara 1 Melanggar Perintah Allah", content)

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_switch_button_and_accordion(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		switch = selenium.find_element_by_id('customSwitch1')
		aktivitas = selenium.find_element_by_id('act')
		pengalaman = selenium.find_element_by_id('exp')
		prestasi = selenium.find_element_by_id('ach')
		switch.send_keys(Keys.RETURN)
		aktivitas.send_keys(Keys.RETURN)
		pengalaman.send_keys(Keys.RETURN)
		prestasi.send_keys(Keys.RETURN)
